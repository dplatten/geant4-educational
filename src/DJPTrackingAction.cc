#include "DJPTrackingAction.h"
#include "DJPUserTrackInformation.h"
#include "DJPTrajectory.h"

#include "G4TrackingManager.hh"


using namespace std;

extern G4bool detailed_output, gui;

DJPTrackingAction::DJPTrackingAction() : G4UserTrackingAction() {
}

void DJPTrackingAction::PreUserTrackingAction(const G4Track *aTrack) {
    // Use custom trajectory class and track information
    fpTrackingManager->SetTrajectory(new DJPTrajectory(aTrack));
    fpTrackingManager->SetUserTrackInformation(new DJPUserTrackInformation);
}

void DJPTrackingAction::PostUserTrackingAction(const G4Track *aTrack) {
    DJPTrajectory *trajectory = (DJPTrajectory *) fpTrackingManager->GimmeTrajectory();
    DJPUserTrackInformation *trackInformation = (DJPUserTrackInformation *) aTrack->GetUserInformation();

    if (detailed_output || gui) {
        for (G4int i = 0; i < trackInformation->GetComptonCount(); i++) {
            trajectory->IncComptons();
        }
        for (G4int i = 0; i < trackInformation->GetPhotoelectricCount(); i++) {
            trajectory->IncPhotoelectrics();
        }
        for (G4int i = 0; i < trackInformation->GetMultipleScatterCount(); i++) {
            trajectory->IncMultipleScatter();
        }
        for (G4int i = 0; i < trackInformation->GeteIonisationCount(); i++) {
            trajectory->InceIonisations();
        }
        for (G4int i = 0; i < trackInformation->GetRayleighCount(); i++) {
            trajectory->IncRayleighs();
        }
    }
    if (!trackInformation->GetIsPrimary()) trajectory->HasInteracted();

    if (trackInformation->GetHasEnteredScoringVolume()) {
        trajectory->HasEnteredScoringVolume();
        trajectory->SetTrackKineticEnergy(trackInformation->GetTrackKineticEnergy());
    }
}
