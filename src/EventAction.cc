#include "EventAction.h"
#include "DJPTrajectory.h"
#include "DJPRunAction.h"
#include "DJPAnalysis.h"

#include "G4RunManager.hh"
#include "G4Event.hh"
#include "G4TrajectoryContainer.hh"
#include "G4SystemOfUnits.hh"

using namespace std;

extern G4bool histogram_output;
extern G4bool per_photon_output;
extern ofstream outputfile;
extern G4bool detailed_output;
extern G4bool binary_output;

EventAction::EventAction()
        : G4UserEventAction() {}

EventAction::~EventAction() {}

void EventAction::BeginOfEventAction(const G4Event *) {}

void EventAction::EndOfEventAction(const G4Event *event) {

    // Get the analysis manager
    auto analysisManager = G4AnalysisManager::Instance();

    // Get the trajectories associated with this event
    G4TrajectoryContainer *traj_container = event->GetTrajectoryContainer();

    G4int n_trajectories = 0;
    if (traj_container) n_trajectories = traj_container->entries();

    // Loop through each trajectory. If any of them have entered the scoring volume then log
    // data to the output file.
    for (G4int i = 0; i < n_trajectories; i++) {
        DJPTrajectory *traj = (DJPTrajectory * )((*(event->GetTrajectoryContainer()))[i]);
        if (traj->GetHasEnteredScoringVolume()) {

            if (histogram_output) {
                // Log the energy in the primary or secondary histogram as appropriate
                if (traj->GetIsPrimary() && traj->GetParentID() == 0)
                    analysisManager->FillH1(0, traj->GetTrackKineticEnergy());
                else if (!traj->GetIsPrimary() && traj->GetParentID() == 0)
                    analysisManager->FillH1(1, traj->GetTrackKineticEnergy());
            }

            if (per_photon_output) {
                if (detailed_output) {
                    if (binary_output) {
                        // Write out as binary
                        G4float energy = traj->GetTrackKineticEnergy() / keV;
                        outputfile.write((char *) &energy, sizeof(G4float));

                        unsigned char small_value = traj->GetComptonCount();
                        outputfile.write((char *) &small_value, sizeof(unsigned char));

                        small_value = traj->GetMultipleScatterCount();
                        outputfile.write((char *) &small_value, sizeof(unsigned char));

                        small_value = traj->GetRayleighCount();
                        outputfile.write((char *) &small_value, sizeof(unsigned char));

                        small_value = traj->GeteIonisationCount();
                        outputfile.write((char *) &small_value, sizeof(unsigned char));

                        small_value = traj->GetPhotoelectricCount();
                        outputfile.write((char *) &small_value, sizeof(unsigned char));

                        small_value = traj->GetParentID();
                        outputfile.write((char *) &small_value, sizeof(unsigned char));

                        small_value = traj->GetIsPrimary();
                        outputfile.write((char *) &small_value, sizeof(unsigned char));

                        std::string particle_name = traj->GetParticleName();
                        G4int size = particle_name.size();
                        outputfile.write(reinterpret_cast<char *>(&size), sizeof(G4int));
                        outputfile.write(particle_name.c_str(), size);
                        //outputfile.write((char *) &size, sizeof(size));
                        //outputfile.write(&particle_name[0], size);
                    } else {
                        // Write out as plain ascii text
                        outputfile << traj->GetTrackKineticEnergy() / keV << "\t"
                                   << traj->GetComptonCount() << "\t"
                                   << traj->GetMultipleScatterCount() << "\t"
                                   << traj->GetRayleighCount() << "\t"
                                   << traj->GeteIonisationCount() << "\t"
                                   << traj->GetPhotoelectricCount() << "\t"
                                   << traj->GetParentID() << "\t"
                                   << traj->GetIsPrimary() << "\t"
                                   << traj->GetParticleName() << G4endl;
                    }
                } else {
                    if (binary_output) {
                        // Write out as binary
                        G4float energy = traj->GetTrackKineticEnergy() / keV;
                        outputfile.write((char *) &energy, sizeof(G4float));
                        unsigned char id = traj->GetParentID();
                        outputfile.write((char *) &id, sizeof(unsigned char));
                        id = traj->GetIsPrimary();
                        outputfile.write((char *) &id, sizeof(unsigned char));
                    } else {
                        // Write out as plain ascii text
                        outputfile << traj->GetTrackKineticEnergy() / keV << "\t"
                                   << traj->GetParentID() << "\t"
                                   << traj->GetIsPrimary()
                                   << G4endl;
                    }
                }
            }
        }
    }
}
