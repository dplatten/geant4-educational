#include "Primaries.h"
#include "G4GeneralParticleSource.hh"
#include "G4Event.hh"

Primaries::Primaries() {
    particleSource = new G4GeneralParticleSource();
}

Primaries::~Primaries() {
    delete particleSource;
}

void Primaries::GeneratePrimaries(G4Event *anEvent) {
    particleSource->GeneratePrimaryVertex(anEvent);
}


