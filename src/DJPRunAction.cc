#include "DJPRunAction.h"
#include "DJPAnalysis.h"

#include "G4Run.hh"
#include "G4RunManager.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

extern G4String outputFileName;
extern G4bool histogram_output;

DJPRunAction::DJPRunAction() : G4UserRunAction() {
    // Set printing event number per each event
    //G4RunManager::GetRunManager()->SetPrintProgress(1);

    if (histogram_output) {
        // Create analysis manager
        auto analysisManager = G4AnalysisManager::Instance();
        G4cout << "Using " << analysisManager->GetType() << G4endl;

        //analysisManager->SetVerboseLevel(1);

        // Create a histogram for primaries and secondaries
        analysisManager->CreateH1("primaries", "Primary photons", 240, 0.0, 120.0 * keV);
        analysisManager->CreateH1("secondaries", "Secondary photons and some electrons", 240, 0.0, 120.0 * keV);
    }
}

DJPRunAction::~DJPRunAction() {
    if (histogram_output)
        delete G4AnalysisManager::Instance();
}

void DJPRunAction::BeginOfRunAction(const G4Run *) {
    if (histogram_output) {
        auto analysisManager = G4AnalysisManager::Instance();

        G4cout << "Opening analysis file" << G4endl;
        G4String fileName = outputFileName + ".csv";
        analysisManager->OpenFile(fileName);
    }
}

void DJPRunAction::EndOfRunAction(const G4Run *) {
    if (histogram_output) {
        auto analysisManager = G4AnalysisManager::Instance();

        G4cout << "Writing analysis file" << G4endl;
        analysisManager->Write();
        analysisManager->CloseFile();
    }
}