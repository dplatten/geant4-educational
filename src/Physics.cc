#include "Physics.h"
#include "G4EmStandardPhysics_option4.hh"

Physics::Physics() {
    defaultCutValue = 0.7 * mm; // This is the default value

    G4int verbosity = 0;
    RegisterPhysics(new G4EmStandardPhysics_option4(verbosity));
}

Physics::~Physics() {
}

void Physics::SetCuts() {
    SetCutsWithDefault();
}

