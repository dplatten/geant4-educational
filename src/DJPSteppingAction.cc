#include "DJPSteppingAction.h"
#include "DJPUserTrackInformation.h"

#include "G4Step.hh"
#include "G4VProcess.hh"
#include "G4ProcessManager.hh"

using namespace std;
extern G4bool detailed_output, gui;


DJPSteppingAction::DJPSteppingAction() : G4UserSteppingAction() {
}

void DJPSteppingAction::UserSteppingAction(const G4Step *theStep) {
    G4StepPoint *postStepPoint = theStep->GetPostStepPoint();

    // Get the track associated with the step
    G4Track *theTrack = theStep->GetTrack();

    // Get the track information
    DJPUserTrackInformation *trackInformation = (DJPUserTrackInformation *) theTrack->GetUserInformation();

    const G4String &postStepProcessName = postStepPoint->GetProcessDefinedStep()->GetProcessName();

    // If this step involved a process (compt, Rayl etc.) then set the HasInteracted
    // flag in the trackInformation and increment the appropriate trackInformation
    // counter.
    if (postStepProcessName != "Transportation") {
        trackInformation->HasInteracted();

        if (detailed_output || gui) {
            if (postStepProcessName == "Rayl") trackInformation->IncMultipleScatters();
            else if (postStepProcessName == "compt") trackInformation->IncComptons();
            else if (postStepProcessName == "msc") trackInformation->IncMultipleScatters();
            else if (postStepProcessName == "eIoni") trackInformation->InceIonisations();
            else if (postStepProcessName == "phot") trackInformation->IncPhotoelectrics();
        }
    }

    // If this step involved the scoring region then set the HasEnteredScoringVolume
    // flag and the track's kinetic energy in trackInformation. The track is then
    // killed off because I'm only interested in it up to the point that it enters
    // the scoring region.
    if (postStepPoint->GetPhysicalVolume() != nullptr) {
        G4String postStepVolName = postStepPoint->GetPhysicalVolume()->GetName();

        if (postStepVolName == "scoring_region_phys") {
            trackInformation->HasEnteredScoringVolume();
            trackInformation->SetTrackKineticEnergy(theTrack->GetKineticEnergy());
            theTrack->SetTrackStatus(fKillTrackAndSecondaries);
        }
    }
}
