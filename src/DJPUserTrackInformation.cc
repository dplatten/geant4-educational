#include "DJPUserTrackInformation.h"

DJPUserTrackInformation::DJPUserTrackInformation() : fComptonScatters(0), fRayleighScatters(0), fMultipleScatters(0),
                                                     feIonisations(0), fphotoelectrics(0), fIsPrimary(true),
                                                     fHasEnteredScoringVolume(false), fTrackKineticEnergy(0) {}

DJPUserTrackInformation::~DJPUserTrackInformation() {}
