#include "DJPTrajectory.h"
#include "G4TrajectoryPoint.hh"
#include "G4Trajectory.hh"
#include "G4VVisManager.hh"
#include "G4Polyline.hh"
#include "G4Polymarker.hh"
#include "G4ParticleTypes.hh"
#include "G4VisAttributes.hh"

G4ThreadLocal G4Allocator<DJPTrajectory> *DJPTrajectoryAllocator = nullptr;


DJPTrajectory::DJPTrajectory()
        : G4Trajectory(), fWls(false), fDrawit(false), fForceNoDraw(false), fForceDraw(false), fComptonScatters(0),
          fRayleighScatters(0), fMultipleScatters(0), feIonisations(0), fphotoelectrics(0), fIsPrimary(true),
          fHasEnteredScoringVolume(false), fTrackKineticEnergy(0) {
    fParticleDefinition = nullptr;
}

DJPTrajectory::DJPTrajectory(const G4Track *aTrack) : G4Trajectory(aTrack), fWls(false), fDrawit(false),
                                                      fComptonScatters(0), fRayleighScatters(0), fMultipleScatters(0),
                                                      feIonisations(0), fphotoelectrics(0), fIsPrimary(true),
                                                      fHasEnteredScoringVolume(false), fTrackKineticEnergy(0) {
    fParticleDefinition = aTrack->GetDefinition();
}

DJPTrajectory::~DJPTrajectory() {}

void DJPTrajectory::DrawTrajectory() const {
    // i_mode is no longer available as an argument of G4VTrajectory.
    // In this exampple it was always called with an argument of 50.
    const G4int i_mode = 50;
    // Consider using commands /vis/modeling/trajectories.

    //Taken from G4VTrajectory and modified to select colours based on particle
    //type and to selectively eliminate drawing of certain trajectories.


//    if (!fForceDraw && (!fDrawit || fForceNoDraw))
//        return;


    // If i_mode>=0, draws a trajectory as a polyline and, if i_mode!=0,
    // adds markers - yellow circles for step points and magenta squares
    // for auxiliary points, if any - whose screen size in pixels is
    // given by std::abs(i_mode)/1000.  E.g: i_mode = 5000 gives easily
    // visible markers.

    G4VVisManager *pVVisManager = G4VVisManager::GetConcreteInstance();
    if (!pVVisManager) return;

    const G4double markerSize = std::abs(i_mode) / 1000;
    G4bool lineRequired(i_mode >= 0);
    G4bool markersRequired(markerSize > 0.);

    G4Polyline trajectoryLine;
    G4Polymarker stepPoints;
    G4Polymarker auxiliaryPoints;

    for (G4int i = 0; i < GetPointEntries(); i++) {
        G4VTrajectoryPoint *aTrajectoryPoint = GetPoint(i);
        const std::vector<G4ThreeVector> *auxiliaries = aTrajectoryPoint->GetAuxiliaryPoints();
        if (auxiliaries) {
            for (size_t iAux = 0; iAux < auxiliaries->size(); ++iAux) {
                const G4ThreeVector pos((*auxiliaries)[iAux]);
                if (lineRequired) {
                    trajectoryLine.push_back(pos);
                }
                if (markersRequired) {
                    auxiliaryPoints.push_back(pos);
                }
            }
        }
        const G4ThreeVector pos(aTrajectoryPoint->GetPosition());
        if (lineRequired) {
            trajectoryLine.push_back(pos);
        }
        if (markersRequired) {
            stepPoints.push_back(pos);
        }
    }


    if (lineRequired) {
        G4Colour colour;

        if (fIsPrimary)
            // Primary are green
            colour = G4Colour(0., 1., 0.);
        else if (fParticleDefinition == G4Electron::ElectronDefinition())
            // All electrons are red
            colour = G4Colour(1., 0., 0.);
        else if (fComptonScatters != 0)
            // At least one Compton scatter is blue
            colour = G4Colour(0., 0., 1.);
        else if (fRayleighScatters != 0)
            // At least one Rayleigh scatter is cyan
            colour = G4Colour(0., 1., 1.);
        else if (fMultipleScatters != 0)
            // At least one multiple scatter is purple
            colour = G4Colour(0.5, 0.5, 1.0);
        else if (fphotoelectrics != 0)
            // At least one photoelectric absorption is pink
            colour = G4Colour(1., 0.5, 1.0);
        else {
            // Anything else is yellow
            colour = G4Colour(1., 1., 0.);
            G4cout << fIsPrimary << "\t" << fComptonScatters << "\t" << fRayleighScatters << "\t" << fMultipleScatters
                   << "\t" << feIonisations << "\t" << fphotoelectrics << G4endl;
        }

        G4VisAttributes trajectoryLineAttribs(colour);
        trajectoryLine.SetVisAttributes(&trajectoryLineAttribs);
        pVVisManager->Draw(trajectoryLine);
    }


    /*
    if (markersRequired) {
        auxiliaryPoints.SetMarkerType(G4Polymarker::squares);
        auxiliaryPoints.SetScreenSize(markerSize);
        auxiliaryPoints.SetFillStyle(G4VMarker::filled);
        G4VisAttributes auxiliaryPointsAttribs(G4Colour(0., 1., 1.));  // Magenta
        auxiliaryPoints.SetVisAttributes(&auxiliaryPointsAttribs);
        pVVisManager->Draw(auxiliaryPoints);

        stepPoints.SetMarkerType(G4Polymarker::circles);
        stepPoints.SetScreenSize(markerSize);
        stepPoints.SetFillStyle(G4VMarker::filled);
        G4VisAttributes stepPointsAttribs(G4Colour(1., 1., 0.));  // Yellow
        stepPoints.SetVisAttributes(&stepPointsAttribs);
        pVVisManager->Draw(stepPoints);
    }
*/
}
