#include "Geometry.h"

#include "G4NistManager.hh"
#include "G4Material.hh"

#include "G4SystemOfUnits.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"

#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4ThreeVector.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

extern G4double filter_thickness;
extern G4String filter_material;
extern G4bool filter_front_at_origin;
extern G4double target_diameter;

Geometry::Geometry() = default;


Geometry::~Geometry() = default;


G4VPhysicalVolume *Geometry::Construct() {
    //------------------------------------------------------------------------------------------------------------------
    // Create a G4NistManager that we can obtain material data from
    G4NistManager *man = G4NistManager::Instance();

    // Create pointers to the materials required in this simulation
    G4Material *Air = man->FindOrBuildMaterial("G4_AIR");

    G4double a, z, density, temp, pressure;
    G4int nel;
    auto Xe = new G4Element("Xenon", "Xe", z=54., a=131.293*g/mole);
    auto *Xe_detector = new G4Material("Xe gas under pressure", density=146.365*mg/cm3, nel=1, kStateGas, temp=273.15*kelvin, pressure=25.0*atmosphere);
    Xe_detector->AddElement(Xe,1);

    G4Material *FilterMaterial;
    if (filter_material == "Xe_detector")
        FilterMaterial = Xe_detector;
    else
        FilterMaterial = man->FindOrBuildMaterial(filter_material);

    if (FilterMaterial == nullptr) {
        G4cout << "Cannot find " << filter_material << " in the G4NistManager; is it a valid G4Material?" << G4endl;
        G4cout << "Available materials are:" <<G4endl;
        man->ListMaterials("all");
        exit(0);
    }
    //------------------------------------------------------------------------------------------------------------------


    //------------------------------------------------------------------------------------------------------------------
    // Create the simulation world - an air-filled box 1.2 x 1.2 x 1.2 m
    G4double world_size = 0.6 * m;
    auto *world_box = new G4Box("world_box", world_size, world_size, world_size);
    auto *world_log = new G4LogicalVolume(world_box, Air, "world_log");

    // Set the visibility attributes of the world
    auto *worldVisAtt = new G4VisAttributes();
    worldVisAtt->SetVisibility(true);
    worldVisAtt->SetForceWireframe(true);
    world_log->SetVisAttributes(worldVisAtt);

    G4VPhysicalVolume *world_phys = new G4PVPlacement(nullptr, G4ThreeVector(), world_log, "world_phys", nullptr, false, 0, false);
    //------------------------------------------------------------------------------------------------------------------


    //------------------------------------------------------------------------------------------------------------------
    if (filter_thickness > 0) {
        G4cout << "Using " << filter_material << " filter " << filter_thickness << " mm thick" << G4endl;

        // Add a filter to the simulation and position it at 0, 0, 0
        G4double filter_side = 150 * mm;
        auto *material_filter = new G4Box("material_filter", filter_side, filter_side, filter_thickness / 2.0);
        //G4Tubs *water_filter = new G4Tubs("water_filter", 0, filter_diameter / 2.0,
        //                               filter_thickness / 2.0, 0, 360 * deg);
        auto *material_filter_log = new G4LogicalVolume(material_filter, FilterMaterial, "water_filter_log");

        // Set the visibility attributes of the filter
        auto *materialFilterVisAtt = new G4VisAttributes(G4Colour::Grey());
        materialFilterVisAtt->SetVisibility(true);
        material_filter_log->SetVisAttributes(materialFilterVisAtt);

        G4double filter_z_offset = 0.0;
        if (filter_front_at_origin)
            filter_z_offset = -filter_thickness/2.0;
        G4VPhysicalVolume *material_filter_phys = new G4PVPlacement(nullptr,
                                                              G4ThreeVector(0, 0, filter_z_offset),
                                                                    material_filter_log, filter_material+"_filter_phys", world_log, false, 0,
                                                              true);
    }
    else
        G4cout << "Zero filter thickness: no filter being used" << G4endl;
    //------------------------------------------------------------------------------------------------------------------


    //------------------------------------------------------------------------------------------------------------------
    // Add a 10 mm diameter air-filled cylinder to score particles; top surface 100 cm from the x-ray source.
    //G4double scoring_region_diameter = 10.0 * mm;
    G4double scoring_region_thickness = 1.0 * um; // 1 micro meter thick
    auto *scoring_region = new G4Tubs("scoring_region", 0, target_diameter / 2.0,
                                        scoring_region_thickness / 2.0, 0, 360 * deg);
    auto *scoring_region_log = new G4LogicalVolume(scoring_region, Air, "scoring_region_log");

    // Set the visibility attributes of the scoring cylinder
    auto *scoring_regionVisAtt = new G4VisAttributes(G4Colour::Red());
    scoring_regionVisAtt->SetVisibility(true);
    scoring_region_log->SetVisAttributes(scoring_regionVisAtt);

    G4VPhysicalVolume *scoring_region_phys = new G4PVPlacement(nullptr, G4ThreeVector(0, 0, -50 * cm -
                                                                                      (scoring_region_thickness / 2.0)),
                                                               scoring_region_log,
                                                               "scoring_region_phys", world_log, false, 0, true);
    //------------------------------------------------------------------------------------------------------------------


    return world_phys;
}
