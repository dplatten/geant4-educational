# README #

## What is this repository for? ##

* This repository contains a Geant4 simulation of an x-ray beam passing through a block of material
* The x-ray beam has a diagnostic radiology energy spectrum
* The beam is a point source, electronically collimated to fall within a circular target at the far side of the material block
* The user can configure the diameter of the circular target
* The block material and thickness can be configured by the user
* The colour of x-rays in the graphical user interface indicate the type of interactions that have taken place

## Requirements to run this code ##

You need a working [Geant4](http://geant4.web.cern.ch/) installation. You could run Geant4
in a [Docker container](https://hub.docker.com/repository/docker/dplatten/geant4), or run
a [container](https://hub.docker.com/repository/docker/dplatten/geant4-educational)
that already includes this simulation. Alternatively, you could use a Raspberry Pi 4 by
following the instructions further down in this file.

# Downloading, compiling and running this simulation code #

The commands in this section must be typed into the command line of a terminal window.

## Download the source code ##

    git clone https://bitbucket.org/dplatten/geant4-educational.git

## Compile the source code ##

### Make a folder for your compiled code ###

    mkdir geant4-educational-build
    cd geant4-educational-build

### Find the path to your Geant4 library folder ###

To compile the source code you need to know the path to your Geant4 installation library
folder. This should be include in the `LD_LIBRARY_PATH` environment variable. Show this using:

    printenv LD_LIBRARY_PATH

The Geant4 library path will look something like this:

    /usr/local/geant4.10.06.p01/lib

or

    /usr/local/geant4.10.06.p01/lib64

Find what yours is, then type:

    ls /whatever/your/library/path/was

replacing `/whatever/your/library/path/was` with your own Geant4 library path. This will show
you a list of files and also a folder named something like `Geant4-10.6.1`. Add this folder
name exactly as it appears on to the library path you found in the previous step to give you
something that looks like: 

    /usr/local/geant4.10.06.p01/lib/Geant4-10.6.1

or

    /usr/local/geant4.10.06.p01/lib64/Geant4-10.6.1

You will need to know this path for the next step

### Compile the code into an executable binary file ###

Replace `/usr/local/geant4.10.06.p01/lib/Geant4-10.6.1` in the command below with the path
you found in the earlier step.

    cmake -DGeant4_DIR=/usr/local/geant4.10.06.p01/lib/Geant4-10.6.1 ../geant4-educational
    make

## Run a simulation ##

    ./sim -h
    ./sim -g -f 200 -n 100

# Using a Raspberry Pi 3 or 4 #

I was surprised to find that this code (and [Geant4](http://geant4.web.cern.ch/))
is useable on a [Raspberry Pi 4](https://www.raspberrypi.org/products/raspberry-pi-4-model-b/).
A colleague has also successfully used this code with Geant4 on a [Raspberry Pi 3](https://www.raspberrypi.org/products/raspberry-pi-3-model-b/).
I have tested this with Raspbian Buster installed onto an SD card using the
[Raspberry Pi Imager](https://www.raspberrypi.org/downloads/). I have also
tested it with the 64-bit beta version of [Raspberry Pi OS](https://www.raspberrypi.org/forums/viewtopic.php?t=275370).
The visualisation works well.

I've put together some files to help compile and install Geant4 10.6 patch 1 on a
Raspberry Pi 3 or 4 so that you can run this code.

The scripts assume that you are logged in as the user `pi`.

Your Pi must have internet access.

The commands in the following sections must be typed into the command line of a terminal window.

## Add some required packages ##

First install some packages required by Geant4 that do not come as standard
with Raspbian. `cmake` is needed to compile the Geant4 source code;
`qt4-dev-tools` is needed to visualise simulations:

    sudo apt install cmake qt4-dev-tools

If you are using the 64-bit Raspberry Pi OS then Qt5 works, so do this instead
(I've not checked to see if Qt5 works on Raspbian):

    sudo apt install cmake qt5-default

## Download this repository ##

Next download this repository to your local computer:

    git clone https://bitbucket.org/dplatten/geant4-educational.git

## Make the bash scripts executable ##

From the root of this repository's source code make the three bash scripts executable:

    cd geant4-educational
    chmod +x geant4_compile_and_install.sh geant4_get_datasets.sh geant4_get_sourcecode.sh 

## Download the Geant4 source code ##

Run each of the scripts in turn. First to download and extract the Geant4 10.6 patch 1 source code:

    ./geant4_get_sourcecode.sh 

## Download and extract the required Geant4 datasets ##

Then to download and extract the required datasets:

    ./geant4_get_datasets.sh

## Compile and install Geant4 ##

Finally compile and install Geant4 (this will take about an hour and a half on a Raspberry Pi 4,
and much longer than that on a Raspberry Pi 3):

    ./geant4_compile_and_install.sh

## Add the required Geant4 environment variables ##

Once the compilation and installation is complete you must edit your bash environment so that it
includes the required Geant4 environment variables. To do this add the following line to the end
of your `~/.bashrc` file:

    source /usr/local/geant4.10.06.p01/bin/geant4.sh

You can use the nano editor to do this. In a terminal type the following to edit the file:

    nano ~/.bashrc

You will need to open a new terminal window for the above change to your `.bashrc` file to take
effect. Geant4 is now installed on your Pi and ready to go, so you can follow the instructions
further up in this file to compile and run this simulation code (the `Compile the source code`
and `Run a simulation` headings).
