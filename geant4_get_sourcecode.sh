#!/bin/sh

echo "Making /home/pi/src folder"
mkdir -p /home/pi/src
cd /home/pi/src

echo "Downloading Geant4.10.06.p01.tar.gz source code"
curl -o /home/pi/src/geant4.10.06.p01.tar.gz http://geant4-data.web.cern.ch/geant4-data/releases/source/geant4.10.06.p01.tar.gz

echo "Extracting the source code into /home/pi/src/"
tar xf /home/pi/src/geant4.10.06.p01.tar.gz

echo "Done"

