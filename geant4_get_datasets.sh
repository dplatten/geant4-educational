#!/bin/sh

echo "Making /home/pi/src/geant4_datasets folder"
mkdir -p /home/pi/src/geant4_datasets


DATASETS="
G4NDL.4.6.tar.gz
G4EMLOW.7.9.1.tar.gz
G4PhotonEvaporation.5.5.tar.gz
G4RadioactiveDecay.5.4.tar.gz
G4SAIDDATA.2.0.tar.gz
G4PARTICLEXS.2.1.tar.gz
G4ABLA.3.1.tar.gz
G4INCL.1.0.tar.gz
G4PII.1.3.tar.gz
G4ENSDFSTATE.2.2.tar.gz
G4RealSurface.2.1.1.tar.gz
"

echo "Downloading and extracting datasets"
for DATASET in $DATASETS; do
	curl -o /home/pi/src/geant4_datasets/$DATASET https://geant4-data.web.cern.ch/geant4-data/datasets/$DATASET
	tar zxf /home/pi/src/geant4_datasets/$DATASET -C /home/pi/src/geant4_datasets
	rm /home/pi/src/geant4_datasets/$DATASET
done

echo "Downloading and extracting datasets complete"

ls /home/pi/src/geant4_datasets

