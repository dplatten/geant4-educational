#ifndef DJPTrajectory_h
#define DJPTrajectory_h 1

#include "G4Trajectory.hh"
#include "G4Allocator.hh"

class DJPTrajectory : public G4Trajectory {
public:

    DJPTrajectory();

    DJPTrajectory(const G4Track *aTrack);

    DJPTrajectory(DJPTrajectory &);

    virtual ~DJPTrajectory();


virtual void DrawTrajectory() const;



    inline void *operator new(size_t);

    inline void operator delete(void *);

    void IncComptons() { fComptonScatters++; }

    void IncRayleighs() { fRayleighScatters++; }

    void IncMultipleScatter() { fMultipleScatters++; }

    void InceIonisations() { feIonisations++; }

    void IncPhotoelectrics() { fphotoelectrics++; }

    G4int GetComptonCount() { return fComptonScatters; }

    G4int GetRayleighCount() { return fRayleighScatters; }

    G4int GetMultipleScatterCount() { return fMultipleScatters; }

    G4int GeteIonisationCount() { return feIonisations; }

    G4int GetPhotoelectricCount() { return fphotoelectrics; }

    G4bool GetIsPrimary() { return fIsPrimary; }

    void HasInteracted() { fIsPrimary = false; }

    G4bool GetHasEnteredScoringVolume() { return fHasEnteredScoringVolume; }

    void HasEnteredScoringVolume() { fHasEnteredScoringVolume = true; }

    inline void SetTrackKineticEnergy(G4double e) { fTrackKineticEnergy = e; };

    inline G4double GetTrackKineticEnergy() { return fTrackKineticEnergy; }

private:

    G4bool fWls;
    G4bool fDrawit;
    G4bool fForceNoDraw;
    G4bool fForceDraw;
    G4ParticleDefinition *fParticleDefinition;
    G4int fComptonScatters;
    G4int fRayleighScatters;
    G4int fMultipleScatters;
    G4int feIonisations;
    G4int fphotoelectrics;
    G4bool fIsPrimary;
    G4bool fHasEnteredScoringVolume;
    G4double fTrackKineticEnergy;
};

extern G4ThreadLocal G4Allocator<DJPTrajectory> *DJPTrajectoryAllocator;

inline void *DJPTrajectory::operator new(size_t) {
    if (!DJPTrajectoryAllocator)
        DJPTrajectoryAllocator = new G4Allocator<DJPTrajectory>;
    return (void *) DJPTrajectoryAllocator->MallocSingle();
}

inline void DJPTrajectory::operator delete(void *aTrajectory) {
    DJPTrajectoryAllocator->FreeSingle((DJPTrajectory *) aTrajectory);
}

#endif
