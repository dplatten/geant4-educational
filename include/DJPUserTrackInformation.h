#ifndef DJPUserTrackInformation_h
#define DJPUserTrackInformation_h 1

#include "G4UserTrackingAction.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4ParticleDefinition.hh"
#include "G4Track.hh"
#include "G4Allocator.hh"
#include "G4VUserTrackInformation.hh"

class DJPUserTrackInformation : public G4VUserTrackInformation {
public:

    DJPUserTrackInformation();

    virtual ~DJPUserTrackInformation();

    void IncComptons() { fComptonScatters++; }

    void IncRayleighs() { fRayleighScatters++; }

    void IncMultipleScatters() { fMultipleScatters++; }

    void InceIonisations() { feIonisations++; }

    void IncPhotoelectrics() { fphotoelectrics++; }

    G4int GetComptonCount() { return fComptonScatters; }

    G4int GetRayleighCount() { return fRayleighScatters; }

    G4int GetMultipleScatterCount() { return fMultipleScatters; }

    G4int GeteIonisationCount() { return feIonisations; }

    G4int GetPhotoelectricCount() { return fphotoelectrics; }

    void HasInteracted() { fIsPrimary = false; }

    G4bool GetIsPrimary() { return fIsPrimary; }

    G4bool GetHasEnteredScoringVolume() { return fHasEnteredScoringVolume; }

    void HasEnteredScoringVolume() { fHasEnteredScoringVolume = true; }

    inline void SetTrackKineticEnergy(G4double e) { fTrackKineticEnergy = e; };

    inline G4double GetTrackKineticEnergy() { return fTrackKineticEnergy; }

    inline virtual void Print() const {};

private:

    G4int fComptonScatters;
    G4int fRayleighScatters;
    G4int fMultipleScatters;
    G4int feIonisations;
    G4int fphotoelectrics;
    G4bool fIsPrimary;
    G4bool fHasEnteredScoringVolume;
    G4double fTrackKineticEnergy;
};

#endif
