#ifndef DJPRunAction_h
#define DJPRunAction_h 1

#include "G4UserRunAction.hh"
#include "globals.hh"

class G4Run;

class DJPRunAction : public G4UserRunAction
{
public:
    DJPRunAction();
    virtual ~DJPRunAction();

    virtual void BeginOfRunAction(const G4Run*);
    virtual void EndOfRunAction(const G4Run*);
};

#endif