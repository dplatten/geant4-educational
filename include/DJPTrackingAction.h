#ifndef DJPTrackingAction_h
#define DJPTrackingAction_h 1

#include "G4UserTrackingAction.hh"

class DJPTrackingAction : public G4UserTrackingAction {
public:
    DJPTrackingAction();

    virtual ~DJPTrackingAction() {};

    virtual void PreUserTrackingAction(const G4Track *);

    virtual void PostUserTrackingAction(const G4Track *);
};

#endif