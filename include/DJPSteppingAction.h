#ifndef DJPSteppingAction_h
#define DJPSteppingAction_h 1

#include "G4UserSteppingAction.hh"

class DJPSteppingAction : public G4UserSteppingAction {
public:
    DJPSteppingAction();

    virtual void UserSteppingAction(const G4Step *);
};

#endif