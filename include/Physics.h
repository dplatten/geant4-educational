#ifndef Physics_h
#define Physics_h 1

#include "G4SystemOfUnits.hh"
#include "G4VModularPhysicsList.hh"
#include "globals.hh"

class Physics : public G4VModularPhysicsList {
public:
    Physics();

    ~Physics();

protected:
    void SetCuts();
};

#endif
