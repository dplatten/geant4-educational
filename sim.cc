#include "EventAction.h"
#include "DJPSteppingAction.h"
#include "DJPTrackingAction.h"
#include "DJPRunAction.h"

#include <math.h>

#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4UIcommand.hh"

#include "Primaries.h"
#include "Geometry.h"
#include "Physics.h"

#include "G4NistManager.hh"

#include "G4SystemOfUnits.hh"

#include "G4VisExecutive.hh"
#include "G4UIExecutive.hh"

#include <fstream>
#include <stdlib.h>
#include <chrono>

using namespace std;
using namespace std::chrono;

G4double filter_thickness;  // The water box thickness in mm
G4String filter_material = "G4_WATER"; // The material to make the filter from
G4bool filter_front_at_origin = false; // Flag to set filter front at coordinate origin
G4bool per_photon_output = false; // Flag to set if user wants output file with each photon energy
G4bool detailed_output = false; // Flag to set if user wants detailed output of each photon
G4bool gui = false; // Flag to set if user wants the gui
G4bool binary_output = false; // Flag to set if user wants to write output in binary
G4bool histogram_output = false; // Flag to set if user wants histograms of primary and secondary photons at scorer
G4double target_diameter; // Diameter of the beam in mm at the scoring plane

// Use current date and time to name output and seed files
system_clock::time_point now = system_clock::now();
time_t tt = system_clock::to_time_t(now);
struct tm t = *localtime(&tt);

char day_time_string[50];
int n = sprintf(day_time_string, "./%4d-%02d-%02d-%02d%02d%02d", t.tm_year + 1900, t.tm_mon + 1, t.tm_mday, t.tm_hour,
                t.tm_min, t.tm_sec);

G4String outputFileName = strcat(day_time_string, "-output.dat");
ofstream outputfile;

static void show_usage() {
    G4cerr << G4endl
           << "sim -n NUMBER -f N -m NAME -s SEED -sa SEED_A -sb SEED_B -hi -d -b -g -lm" << G4endl
           << G4endl
           << "Options:" << G4endl
           << "\t-h,--help\t\tShow this help message and exit (optional)" << G4endl
           << "\t-g,--gui\t\tSet to show the graphical user interface (optional)" << G4endl
           << "\t-n,--n_particles NUMBER\tSpecify the number (NUMBER) of primary particles (optional, default 0)" << G4endl
           << "\t-f,--filter N\t\tSpecify material box thickness in mm (optional, default 0)" << G4endl
           << "\t-m,--material NAME\tSpecify the material that the box should be made from (optional, default G4_WATER)" << G4endl
           << "\t-lm,--list_materials\tList the available materials and exit (optional)" << G4endl
           << "\t-fc,--front-centre\tPlace the front of the filter block at the centre of the geometry (optional)" << G4endl
           << "\t-td,--target-diameter\tSet the diameter of the x-ray beam at the scoring plane (optional)" << G4endl
           << "\t-hi,--histogram_output\tSet to output energy histogram for primary and secondary photons reaching the scoring plane (optional)" << G4endl
           << "\t-p,--per_photon\t\tSet to output result for every individual photon (optional)" << G4endl
           << "\t-d,--detailed\t\tSet to output detailed results for each photon (optional)" << G4endl
           << "\t-b,--binary\t\tWrite individual photon output as binary rather than ascii (smaller file size) (optional)" << G4endl
           << "\t-s,--seed SEED\t\tSpecify the random number SEED to use (optional, default based on system time)" << G4endl
           << "\t-sa,--seed_a SEED_A\tSpecify the first of two random number seeds, SEED_A (optional)" << G4endl
           << "\t-sb,--seed_b SEED_B\tSpecify the second of two random number seeds, SEED_B (optional)" << G4endl
           << G4endl
           << "Using -s SEED passes SEED to CLHEP::HepRandom::setTheSeed(SEED)" << G4endl
           << "Using -sa SEED_A -sb SEED_B passes the values to CLHEP::HepRandom::setTheSeeds(the_seeds) as a 2-element array" << G4endl
           << "Using -s overrides any individual seed values provided via -sa and -sb" << G4endl
           << G4endl
           << "A list of valid materials to use with -m can be found in the Geant4 material database (google.com/#q=geant4+material+database)" << G4endl
           << G4endl
           << "Using -p results in an ascii output file containing the kinetic energy, parent ID and primary status of "
              "each trajectory that enters the scoring region." << G4endl
           << G4endl
           << "Using -d together with -p includes the number of Compton, multiple scatter, Rayleigh, e-ionisation and "
              "photoelectric interactions for each trajectory as well as the default data." << G4endl
           << G4endl
           << "Using -m with Xe_detector will use Xe gas at 25 atm pressure to simulate a CT gas detector." << G4endl
           << G4endl
           << "Green: primary photon (no interactions)" << G4endl
           << "Red: an electron" << G4endl
           << "Blue: at least one Compton scatter" << G4endl
           << "Cyan: at least one Rayleigh scatter" << G4endl
           << "Purple: at least one multiple scatter" << G4endl
           << "Pink: at least one photoelectric absorption" << G4endl
           << "Yellow: anything else" << G4endl;
}


int main(int argc, char *argv[]) {
    G4String n_particles = "0";  // The number of particles to simulate
    G4int seed = 0, seed_a = 0, seed_b = 0;  // The random number seeds
    target_diameter = 10.0 * mm;

    // Parse the command line parameters
    for (int i = 1; i < argc; ++i) {

        if (std::string(argv[i]) == "--help" || std::string(argv[i]) == "-h") {
            show_usage();
            return 0;
        } else if (std::string(argv[i]) == "--n_particles" || std::string(argv[i]) == "-n") {
            if (i + 1 < argc) { // Make sure we aren't at the end of argv
                n_particles = argv[i + 1];
            }
        } else if (std::string(argv[i]) == "--filter" || std::string(argv[i]) == "-f") {
            if (i + 1 < argc) { // Make sure we aren't at the end of argv
                filter_thickness = G4UIcommand::ConvertToDouble(argv[i+1]) / mm;
            }
        } else if (std::string(argv[i]) == "--material" || std::string(argv[i]) == "-m") {
            if (i + 1 < argc) { // Make sure we aren't at the end of argv
                filter_material = argv[i + 1];
            }
        } else if (std::string(argv[i]) == "--list-materials" || std::string(argv[i]) == "-lm") {
                G4NistManager::Instance()->ListMaterials("all");
                return 0;
        } else if (std::string(argv[i]) == "--front-centre" || std::string(argv[i]) == "-fc") {
            filter_front_at_origin = true;
        } else if (std::string(argv[i]) == "--seed" || std::string(argv[i]) == "-s") {
            if (i + 1 < argc) { // Make sure we aren't at the end of argv
                seed = G4UIcommand::ConvertToInt(argv[i+1]);
            }
        } else if (std::string(argv[i]) == "--seed_a" || std::string(argv[i]) == "-sa") {
            if (i + 1 < argc) { // Make sure we aren't at the end of argv
                seed_a = G4UIcommand::ConvertToInt(argv[i+1]);
            }
        } else if (std::string(argv[i]) == "--seed_b" || std::string(argv[i]) == "-sb") {
            if (i + 1 < argc) { // Make sure we aren't at the end of argv
                seed_b = G4UIcommand::ConvertToInt(argv[i+1]);
            }
        } else if (std::string(argv[i]) == "--gui" || std::string(argv[i]) == "-g") {
            gui = true;
        } else if (std::string(argv[i]) == "--per_photon" || std::string(argv[i]) == "-p") {
            per_photon_output = true;
        } else if (std::string(argv[i]) == "--detailed" || std::string(argv[i]) == "-d") {
            detailed_output = true;
        } else if (std::string(argv[i]) == "--binary" || std::string(argv[i]) == "-b") {
            binary_output = true;
        } else if (std::string(argv[i]) == "--histogram_output" || std::string(argv[i]) == "-hi") {
            histogram_output = true;
        } else if (std::string(argv[i]) == "--target_diameter" || std::string(argv[i]) == "-td") {
            target_diameter = G4UIcommand::ConvertToInt(argv[i+1]) * mm;
        }

    }

    if (per_photon_output)
        outputfile.open(outputFileName, ios::binary | ios::out);

    // Instantiate a Ranecu random engine
    CLHEP::HepRandom::setTheEngine(new CLHEP::RanecuEngine);

    // If either the seed or either of the user-defined seeds are unset then use the system time
    if (seed == 0 && (seed_a == 0 || seed_b == 0)) {
        // Get the current system time in seconds since 1/1/1970 UTC and use it as the seed
        G4cout << "Using system time to set the seed" << G4endl;
        seed = std::chrono::duration_cast<std::chrono::seconds>(
                std::chrono::system_clock::now().time_since_epoch()).count();
        CLHEP::HepRandom::setTheSeed(seed);
    }
    // Else if seed is non-zero then use it to generate the two seed values
    else if (seed != 0) {
        G4cout << "Using the user-provided value to set the seed" << G4endl;
        CLHEP::HepRandom::setTheSeed(seed);
    }
        // Else use the two user-supplied seed values
    else {
        G4cout << "Using the two user-provided values to set the individual seeds" << G4endl;
        long the_seeds[2] = {seed_a, seed_b};
        CLHEP::HepRandom::setTheSeeds(the_seeds);
    }

    // Print the seed and associated seed values on the screen
    const long *the_seeds = CLHEP::HepRandom::getTheSeeds();
    G4cout << "\nSeed is: " << seed << G4endl;
    G4cout << "\nInternal random number seeds are: " << the_seeds[0] << "; " << the_seeds[1] << G4endl;

    if (per_photon_output || histogram_output) {
        // Write the seed information to a file if results are being output
        const std::string seedFileName = strcat(day_time_string, "-seeds.txt");
        ofstream seed_file(seedFileName, ios::out | ios::app);
        seed_file << "\nSeed is: " << seed << G4endl;
        seed_file << "\nInternal random number seeds are: " << the_seeds[0] << "; " << the_seeds[1] << G4endl;
        seed_file.close();
    }


    auto *runManager = new G4RunManager;

    G4VModularPhysicsList *physics = new Physics();
    runManager->SetUserInitialization(physics);

    G4UserRunAction *runAction = new DJPRunAction();
    runManager->SetUserAction(runAction);

    G4VUserDetectorConstruction *detector = new Geometry();
    runManager->SetUserInitialization(detector);

    G4VUserPrimaryGeneratorAction *primaries = new Primaries();
    runManager->SetUserAction(primaries);

    G4UserEventAction *eventAction = new EventAction();
    runManager->SetUserAction(eventAction);

    G4UserSteppingAction *stepAction = new DJPSteppingAction();
    runManager->SetUserAction(stepAction);

    G4UserTrackingAction *trackAction = new DJPTrackingAction();
    runManager->SetUserAction(trackAction);

    if (per_photon_output) {
        // Write out the number of photons; write headings if not in binary mode
        if (binary_output) {
            // Write out the number of primary particles as binary with no headings
            G4int n_particles_value;
            try {
                n_particles_value = std::stoi(n_particles);
            } catch (std::invalid_argument const &e) { // this error is thrown if n_particles is an empty string
                n_particles_value = 0;
            }
            outputfile.write((char *) &n_particles_value, sizeof(G4int));
        } else if (detailed_output) {
            // Write out as plain ascii text including headings
            outputfile << n_particles << "\tphotons" << G4endl;
            outputfile << "TrackKineticEnergy_keV\tn_Compton\tn_multipleScatter\tn_Rayleigh\t"
                       << "n_eIonisation\tn_photoelectric\tparentID\tisPrimary\tparticleName"
                       << G4endl;
        } else {
            // Write out as plain ascii text
            outputfile << n_particles << "\tphotons" << G4endl;
            outputfile << "TrackKineticEnergy_keV\tparentID\tisPrimary" << G4endl;
        }
    }

    runManager->Initialize();

    G4UImanager *UImanager = G4UImanager::GetUIpointer();

    UImanager->ApplyCommand("/control/execute source.mac");
    // Change the source angle
    G4double angle = atan( (target_diameter/2.0) / (1000.0 *mm) );
    UImanager->ApplyCommand("/gps/ang/maxtheta " + G4UIcommand::ConvertToString(angle) + " rad");


    // Force the storing of trajectories so that I can obtain interaction information even when running in non-graphical mode
    UImanager->ApplyCommand("/tracking/storeTrajectory 1");


    if (gui) {
        // Run with the gui
        G4VisManager *visManager = new G4VisExecutive;
        visManager->Initialize();
        auto *ui = new G4UIExecutive(argc, argv);
        UImanager->ApplyCommand("/control/execute vis.mac");
        if (!n_particles.empty()) {
            G4String command = "/run/beamOn ";
            UImanager->ApplyCommand(command + n_particles);
        }

        G4cout << filter_thickness << " mm " << filter_material << " filter" << G4endl
               << "Green:\tprimary photon (no interactions)" << G4endl
               << "Red:\tan electron" << G4endl
               << "Blue:\tat least one Compton scatter" << G4endl
               << "Cyan:\tat least one Rayleigh scatter" << G4endl
               << "Purple:\tat least one multiple scatter" << G4endl
               << "Pink:\tat least one photoelectric absorption" << G4endl
               << "Yellow:\tanything else" << G4endl;
        ui->SessionStart();
        delete ui;
        delete visManager;
    } else {
        // Run in non-graphical mode
        time_t start_time = system_clock::to_time_t(system_clock::now());

        G4String command = "/run/beamOn ";
        UImanager->ApplyCommand(command + n_particles);

        time_t end_time = system_clock::to_time_t(system_clock::now());
        G4int duration = (end_time - start_time);
        if (n_particles != "0") {
            G4double time_per_particle = (G4double) duration / std::stoi(n_particles);
            G4cout << duration << " seconds simulation time" << G4endl;
            G4cout << time_per_particle << " seconds per x-ray" << G4endl;
        }
        else
            G4cout << "Zero particles simulated" << G4endl;
    }


    delete runManager;

    // Close the output file if it was opened
    if (outputfile.is_open())
        outputfile.close();

    return 0;
}
