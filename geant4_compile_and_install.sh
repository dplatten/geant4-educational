#!/bin/sh

echo "Making ~/src/geant4.10.06.p01-build folder"
mkdir -p /home/pi/src/geant4.10.06.p01-build

echo "Creating make file"
cd /home/pi/src/geant4.10.06.p01-build
cmake -DCMAKE_INSTALL_PREFIX=/usr/local/geant4.10.06.p01 -DGEANT4_BUILD_MULTITHREADED=ON -DGEANT4_INSTALL_DATADIR=/home/pi/src/geant4_datasets -DGEANT4_USE_QT=ON /home/pi/src/geant4.10.06.p01

echo "Compiling - this will take several hours on a Raspberry Pi 4, and much longer on an earlier version"
make -j4

echo "Installing Geant4 to /usr/local/geant4.10.06.p01"

sudo make install


echo "
Edit your bash environment so that it includes the required Geant4 environment variables

Add the following line to the end of your ~/.bashrc file:
source /usr/local/geant4.10.06.p01/bin/geant4.sh

You can use the nano editor to do this. In a terminal type the following to edit the file:
nano ~/.bashrc 

You will need to open a new terminal window for the above change to your .bashrc file to take effect


Geant4 is now installed and ready to go - compile and build example B1

	cd src/
	mkdir B1-build
	cd B1-build/
	cmake -DGeant4_DIR=/usr/local/geant4.10.06.p01/lib/Geant4-10.6.1 /usr/local/geant4.10.06.p01/share/Geant4-10.6.1/examples/basic/B1
	make -j4
	./exampleB1


Or download, compile and build this x-ray imaging demonstration

	cd /home/pi/src
	
	mkdir geant4-imaging-demo-build
	cd geant4-imaging-demo-build
	cmake -DGeant4_DIR=/usr/local/geant4.10.06.p01/lib/Geant4-10.6.1 ../geant4-imaging-demo
	make -j4
	./imaging-demo -g -inc_cp
"

